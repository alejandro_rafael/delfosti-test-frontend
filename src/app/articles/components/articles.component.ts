import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../services/articles.service';
import { ResponseApi } from '../interfaces/rquest_response';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})

export class ArticlesComponent implements OnInit {

  public articles: any = []
  public search_article_name: string = ''
  private tiempo: any = ''

  constructor(private articleService: ArticlesService){ }

  ngOnInit(): void {
    this.articleService.getall().subscribe(response => {
      this.articles = response.data
    });
  }

  searchArticles(article: string){
    clearTimeout(this.tiempo)
    this.tiempo = setTimeout(() => {
      this.articleService.search(article).subscribe(response => {
        this.articles = response.data
      });
    }, 500);
  }
}
