import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from "rxjs";
import { ResponseApi } from '../interfaces/rquest_response';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  private url_list: string = "http://localhost:1994/api/v1/articles"
  private url_search: string = "http://localhost:1994/api/v1/articles/search?article="
  constructor(private http: HttpClient) {}

  public getall(): Observable<ResponseApi>{
    return this.http.get<ResponseApi>(this.url_list)
  }

  public search(name: string): Observable<ResponseApi>{
    return this.http.get<ResponseApi>(this.url_search + name)
  }
}
