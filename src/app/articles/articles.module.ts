import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesComponent } from './components/articles.component';
import { BreakPipe } from '../pipes/break.pipe';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ArticlesComponent,
    BreakPipe
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ArticlesComponent
  ]
})
export class ArticlesModule { }
