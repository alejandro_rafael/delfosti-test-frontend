export interface ResponseApi {
    data:  Article[];
    links: Links;
    count: number;
}

interface Article {
    id:         number;
    attributes: Attributes;
    links:      Links;
}

interface Attributes {
    name:        string;
    description: string;
    status:      number;
    categories:  Category[];
}

interface Category {
    name: string;
}

interface Links {
    self: string;
}
